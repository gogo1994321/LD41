using System.Collections.Generic;
using System.Linq;

namespace LD41.Assets.Script
{
    public static class CompareLists
    {
        public static bool ContainsAllItems<T>(IEnumerable<T> a, IEnumerable<T> b)
        {
            if (a != null && b != null)
                return !b.Except(a).Any();

            return true;
        }
    }
}