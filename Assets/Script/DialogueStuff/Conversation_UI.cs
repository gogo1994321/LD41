using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace LD41.Assets.Script
{
    public class Conversation_UI : MonoBehaviour
    {
        public Text mainText;
        [Space(10)]
        public Button choice1Button;
        public Text choice1Text;
        public GameObject lock01;
        [Space(10)]
        public Button choice2Button;
        public Text choice2Text;
        public GameObject lock02;
        [Space(10)]
        public Button choice3Button;
        public Text choice3Text;
        public GameObject lock03;
        [Space(10)]
        public Button choice4Button;
        public Text choice4Text;
        public GameObject lock04;

        [HideInInspector]
        public ConversationManager conversationManager;

        public void ConfigureUI(Dialogue dialogue, List<int> collectedKeys)
        {
            choice1Button.onClick.RemoveAllListeners();
            choice2Button.onClick.RemoveAllListeners();
            choice3Button.onClick.RemoveAllListeners();
            choice4Button.onClick.RemoveAllListeners();

            if (!string.IsNullOrEmpty(dialogue.description))
                mainText.text = dialogue.description;

            if (dialogue.choices.Count > 0)
            {
                bool unlocked = CompareLists.ContainsAllItems<int>(collectedKeys, dialogue.choices[0].keyholes);
                choice1Text.text = unlocked ? dialogue.choices[0].description : "";
                choice1Button.gameObject.SetActive(true);
                //Check if all keys from keyhole list are in the collected keys
                choice1Button.interactable = unlocked;
                choice1Button.onClick.AddListener(() => ButtonClicked(dialogue.choices[0].nextConversationID, dialogue.choices[0].keys));
                lock01.SetActive(!unlocked);
            }
            else
            {
                choice1Button.gameObject.SetActive(false);
            }

            if (dialogue.choices.Count > 1)
            {
                bool unlocked = CompareLists.ContainsAllItems<int>(collectedKeys, dialogue.choices[1].keyholes);
                choice2Text.text = unlocked ? dialogue.choices[1].description : "";
                choice2Button.gameObject.SetActive(true);
                choice2Button.interactable = unlocked;
                choice2Button.onClick.AddListener(() => ButtonClicked(dialogue.choices[1].nextConversationID, dialogue.choices[1].keys));
                lock02.SetActive(!unlocked);
            }
            else
            {
                choice2Button.gameObject.SetActive(false);
            }

            if (dialogue.choices.Count > 2)
            {
                bool unlocked = CompareLists.ContainsAllItems<int>(collectedKeys, dialogue.choices[2].keyholes);
                choice3Text.text = unlocked ? dialogue.choices[2].description : "";
                choice3Button.gameObject.SetActive(true);
                choice3Button.interactable = unlocked;
                choice3Button.onClick.AddListener(() => ButtonClicked(dialogue.choices[2].nextConversationID, dialogue.choices[2].keys));
                lock03.SetActive(!unlocked);
            }
            else
            {
                choice3Button.gameObject.SetActive(false);
            }

            if (dialogue.choices.Count > 3)
            {
                bool unlocked = CompareLists.ContainsAllItems<int>(collectedKeys, dialogue.choices[3].keyholes);
                choice4Text.text = unlocked ? dialogue.choices[3].description : "";
                choice4Button.gameObject.SetActive(true);
                choice4Button.interactable = unlocked;
                choice4Button.onClick.AddListener(() => ButtonClicked(dialogue.choices[3].nextConversationID, dialogue.choices[3].keys));
                lock04.SetActive(!unlocked);
            }
            else
            {
                choice4Button.gameObject.SetActive(false);
            }
        }

        void ButtonClicked(int nextDialogueIndex, List<int> newKeys)
        {
            if (newKeys != null)
                conversationManager.addNewKeys(newKeys);
            conversationManager.loadDialogue(index: nextDialogueIndex);
        }
    }
}