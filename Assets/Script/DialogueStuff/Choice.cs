using System.Collections.Generic;

namespace LD41.Assets.Script
{
    [System.Serializable]
    public class Choice
    {
        public string description;
        public int nextConversationID;
        public List<int> keys;
        public List<int> keyholes;


        public Choice(string description, int nextConversationID, List<int> keys = null, List<int> keyholes = null)
        {
            this.description = description;
            this.nextConversationID = nextConversationID;
            this.keys = keys;
            this.keyholes = keyholes;
        }
    }
}