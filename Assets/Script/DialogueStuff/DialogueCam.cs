﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueCam : MonoBehaviour
{

    [HideInInspector]
    public List<GameObject> cameraPositions = new List<GameObject>();

    void Start() { }

    public void ChangeCameraAngle()
    {
        GameObject newAngle = cameraPositions[Random.Range(0, cameraPositions.Count)];
        Camera.main.transform.position = newAngle.transform.position;
        Camera.main.transform.rotation = newAngle.transform.rotation;
    }
}
