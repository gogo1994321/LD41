﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
namespace LD41.Assets.Script
{
    public class ConversationManager : MonoBehaviour
    {
        /// <summary>
        /// Holds all dialogues of all characters. First key is character name. Second is dialogue ID.
        /// </summary>
        Dictionary<string, Dictionary<int, Dialogue>> characterDialogues = new Dictionary<string, Dictionary<int, Dialogue>>();

        string currentNPCName;
        Dialogue currentDialogue;

        List<int> keysCollected = new List<int>();

        public Conversation_UI conversation_UI;

        void Start()
        {
            conversation_UI.conversationManager = this;
            conversation_UI.gameObject.SetActive(false);

            characterDialogues.Add("Amanda", JsonConvert.DeserializeObject<Dictionary<int, Dialogue>>(Resources.Load<TextAsset>("Amanda").ToString()));
            characterDialogues.Add("Dracula", JsonConvert.DeserializeObject<Dictionary<int, Dialogue>>(Resources.Load<TextAsset>("Dracula").ToString()));
            characterDialogues.Add("InvisibleMan", JsonConvert.DeserializeObject<Dictionary<int, Dialogue>>(Resources.Load<TextAsset>("InvisibleMan").ToString()));
            characterDialogues.Add("Kong", JsonConvert.DeserializeObject<Dictionary<int, Dialogue>>(Resources.Load<TextAsset>("Kong").ToString()));
            characterDialogues.Add("Mummy", JsonConvert.DeserializeObject<Dictionary<int, Dialogue>>(Resources.Load<TextAsset>("Mummy").ToString()));
            characterDialogues.Add("Pennywise", JsonConvert.DeserializeObject<Dictionary<int, Dialogue>>(Resources.Load<TextAsset>("Pennywise").ToString()));
            characterDialogues.Add("Twins", JsonConvert.DeserializeObject<Dictionary<int, Dialogue>>(Resources.Load<TextAsset>("Twins").ToString()));
            characterDialogues.Add("FrankensteinBody", JsonConvert.DeserializeObject<Dictionary<int, Dialogue>>(Resources.Load<TextAsset>("FrankensteinBody").ToString()));

            foreach (KeyValuePair<string, Dictionary<int, Dialogue>> KeyValuePair in characterDialogues)
            {
                ResolveChoiceCopies(characterDialogues[KeyValuePair.Key]);
            }
            CheckKeyCompletion();
        }
        //Run through all keys and see if they have a hole, and vica versa. This is for the devs as a debugging tool
        void CheckKeyCompletion()
        {
            List<int> keys = new List<int>();
            List<int> locks = new List<int>();

            //Fill lists
            foreach (string npc in characterDialogues.Keys)
            {
                foreach (KeyValuePair<int, Dialogue> dialogue in characterDialogues[npc])
                {
                    if (dialogue.Value.choices == null)
                        Debug.Log("NO CHOICES: " + dialogue.Value.description);

                    foreach (Choice choice in dialogue.Value.choices)
                    {
                        if (choice.keys != null)
                            keys.AddRange(choice.keys);

                        if (choice.keyholes != null)
                            locks.AddRange(choice.keyholes);
                    }
                }
            }
            //Make unique
            keys = keys.Distinct().ToList();
            locks = locks.Distinct().ToList();
            //Check lists with each other
            foreach (int key in keys.ToList())
            {
                foreach (int locky in locks.ToList())
                {
                    if (key == locky)
                    {
                        keys.Remove(key);
                        locks.Remove(locky);
                    }
                }
            }

            foreach (int key in keys)
            {
                Debug.LogWarning("Key not used: " + key);
            }
            foreach (int locky in locks)
            {
                Debug.LogWarning("Lock not used: " + locky);
            }
        }

        public void loadDialogue(string npcName = "", int index = 1)
        {
            if (!string.IsNullOrEmpty(npcName))
                currentNPCName = npcName;

            currentDialogue = characterDialogues[currentNPCName][index];
            conversation_UI.gameObject.SetActive(true);
            conversation_UI.ConfigureUI(currentDialogue, keysCollected);
            Camera.main.GetComponent<DialogueCam>().ChangeCameraAngle();
        }

        public void EndDialogue()
        {
            conversation_UI.gameObject.SetActive(false);
        }

        public void addNewKeys(List<int> newKeys)
        {
            keysCollected.AddRange(newKeys);
            keysCollected = keysCollected.Distinct().ToList();  //Make entries unique
        }

        public void overrideKeys(List<int> newKeys)
        {
            keysCollected = new List<int>(newKeys);
        }

        public List<int> getKeys()
        {
            return keysCollected;
        }

        void ResolveChoiceCopies(Dictionary<int, Dialogue> dialogues)
        {
            //Look through all entries
            foreach (KeyValuePair<int, Dialogue> i in dialogues)
            {
                //If we found an entry that needs to copy
                if (i.Value.copyChoicesFrom != -1)
                {
                    //Find the entry it has to copy from
                    foreach (KeyValuePair<int, Dialogue> j in dialogues)
                    {
                        if (i.Value.copyChoicesFrom == j.Key)
                        {
                            //Lets copy over all the choices
                            i.Value.choices = new List<Choice>(j.Value.choices);
                        }
                    }
                }
            }
        }
    }
}