using System.Collections.Generic;

namespace LD41.Assets.Script
{
    [System.Serializable]
    public class Dialogue
    {
        public string description;
        //If wanted to, we can copy the choices from a previous dialogue into this by setting this variable
        public int copyChoicesFrom = -1;
        public List<Choice> choices;

        public Dialogue(string description, List<Choice> choices)
        {
            this.description = description;
            this.choices = choices;
        }
    }
}