namespace LD41.Assets.Script
{
    public interface IUse
    {
        void Use();
        void FinishUse();
    }
}