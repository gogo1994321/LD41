using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using UnityEngine.SceneManagement;
using System;

namespace LD41.Assets.Script.SaveLoad
{
    [System.Serializable]
    public class SaveFile
    {
        public List<int> keys;
        public Vector3 playerLocation;

        public void SaveToFile(GameObject Player)
        {
            keys = Player.GetComponent<ConversationManager>().getKeys();
            playerLocation = Player.transform.position;
            Debug.Log(Environment.GetFolderPath(Environment.SpecialFolder.Personal));
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\save.json", JsonConvert.SerializeObject(this));
        }

        public void LoadFromFile(GameObject Player)
        {
            try
            {
                SaveFile temp = JsonConvert.DeserializeObject<SaveFile>(File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\save.json"));
                if (temp != null)
                {
                    Player.transform.position = temp.playerLocation;
                    Player.GetComponent<ConversationManager>().overrideKeys(temp.keys);
                }
            }
            catch (Exception e)
            {
                if (playerLocation != Vector3.zero)
                    Player.transform.position = playerLocation;
            }
        }
    }
}