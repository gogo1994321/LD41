﻿using System;
using System.Collections.Generic;
using UnityEngine;
namespace LD41.Assets.Script.SaveLoad
{
    [RequireComponent(typeof(BoxCollider))]
    public class SaveLoadTrigger : MonoBehaviour
    {
        public enum TriggerType
        {
            Checkpoint,
            Deadzone
        }
        public TriggerType triggerType;
        public event EventHandler playerEntered;
        void OnTriggerEnter(Collider collider)
        {

            if (collider.tag == "Player" && playerEntered != null)
            {

                playerEntered.Invoke(this, null);
            }
        }
    }
}
