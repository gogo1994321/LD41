using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
namespace LD41.Assets.Script.SaveLoad
{
    public class SaveManager : MonoBehaviour
    {
        public List<SaveLoadTrigger> triggers;
        SaveFile saveFile;

        public Image deathScreen;
        public Text deathText;
        public float fadeTime;

        bool locked;

        void Start()
        {
            saveFile = new SaveFile();
            //triggers = GameObject.FindObjectsOfType<SaveLoadTrigger>().ToList();
            //Debug.Log("Triggers: " + triggers.Count);
            foreach (SaveLoadTrigger trigger in triggers)
            {
                if (trigger.triggerType == SaveLoadTrigger.TriggerType.Checkpoint)
                {
                    trigger.playerEntered += new EventHandler(Save);
                }
                else if (trigger.triggerType == SaveLoadTrigger.TriggerType.Deadzone)
                {
                    trigger.playerEntered += new EventHandler(Load);
                }
            }
            deathScreen.gameObject.SetActive(false);
            //We want to load the file first if found, not overwrite it -.-
            saveFile.LoadFromFile(this.gameObject);
        }

        void Save(object caller, EventArgs e)
        {
            saveFile.SaveToFile(this.gameObject);
        }

        void Load(object caller, EventArgs e)
        {
            if (!locked)
                StartCoroutine(LoadTimer());
        }
        IEnumerator LoadTimer()
        {
            locked = true;
            deathScreen.gameObject.SetActive(true);
            float timer = 0;
            while (timer < fadeTime)
            {
                timer += Time.deltaTime;
                Color c = deathScreen.color;
                Color c2 = deathText.color;
                c.a = Mathf.Clamp01(timer / fadeTime);
                c2.a = c.a;
                deathScreen.color = c;
                deathText.color = c2;
                yield return null;
            }
            timer = 0;
            saveFile.LoadFromFile(this.gameObject);
            yield return new WaitForSeconds(1f);
            while (timer < fadeTime)
            {
                timer += Time.deltaTime;
                Color c = deathScreen.color;
                Color c2 = deathText.color;
                c.a = 1.0f - Mathf.Clamp01(timer / fadeTime);
                c2.a = c.a;
                deathScreen.color = c;
                deathText.color = c2;
                yield return null;
            }
            deathScreen.gameObject.SetActive(false);
            locked = false;
        }
    }
}