using System.Collections.Generic;
using UnityEngine;
namespace LD41.Assets.Script
{
    public class NPC : MonoBehaviour, IUse
    {
        public string npcName;
        public List<GameObject> cameraPositions;

        //Have we talked to this NPC before?
        [HideInInspector]
        bool introduced;

        Animator animator;
        public int talkAnimationCount = 2;
        ConversationManager conversationManager;

        void Start()
        {
            conversationManager = FindObjectOfType<ConversationManager>();
            animator = GetComponent<Animator>();
        }

        public void Use()
        {
            conversationManager.loadDialogue(npcName, introduced ? 1 : 0);
            if (animator)
            {
                animator.SetBool("Talking", true);
                animator.SetInteger("variation", Random.Range(0, talkAnimationCount));
            }
            introduced = true;
        }
        public void FinishUse()
        {
            if (animator)
            {
                animator.SetBool("Talking", false);
                animator.SetInteger("variation", Random.Range(0, talkAnimationCount));
            }
        }

    }
}