﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{

    Animator animationController;

    public float waitAfterAttack = 1f;
    public float waitBeforeAttack = 3f;

    public GameObject deadzone;

    void Start()
    {
        animationController = GetComponent<Animator>();
        deadzone.SetActive(false);
        StartCoroutine(Attack());
    }

    IEnumerator Attack()
    {
        float timer = 0;
        while (timer < waitBeforeAttack)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        animationController.SetTrigger("Attack");
        yield return new WaitForSeconds(0.25f);
        deadzone.SetActive(true);
        yield return StartCoroutine(Retract());
    }

    IEnumerator Retract()
    {
        float timer = 0;
        while (timer < waitAfterAttack)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        animationController.SetTrigger("Retract");
        deadzone.SetActive(false);
        yield return StartCoroutine(Attack());
    }
}
