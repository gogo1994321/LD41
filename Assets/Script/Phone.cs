﻿using System.Collections;
using System.Collections.Generic;
using LD41.Assets.Script;
using UnityEngine;

public class Phone : MonoBehaviour, IUse
{
    public GameObject accuseUI;

    public void FinishUse()
    {
        Debug.Log("Accuse finish");
        accuseUI.SetActive(false);
    }

    public void Use()
    {
        accuseUI.SetActive(true);
    }
}
