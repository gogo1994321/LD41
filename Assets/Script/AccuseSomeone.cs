using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
namespace LD41.Assets.Script
{
    public class AccuseSomeone : MonoBehaviour
    {
        public Image Try;
        int tries = 2;
        public void Accuse(string npcName)
        {
            if (npcName == "InvisibleMan")
            {
                SceneManager.LoadScene("winScene");
            }
            else
            {
                tries--;
                switch (tries)
                {
                    case 1:
                        Try.gameObject.SetActive(false);
                        break;

                    case 0:
                        SceneManager.LoadScene("loseScene");
                        break;
                }
            }
        }
    }
}