﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LD41.Assets.Script
{
    public class PlayerInput : MonoBehaviour
    {

        public float speed = 6f;            // The speed that the player will move at.
        public float jumpForce = 5f;
        public float interactMaxDistance = 2.5f;

        Vector3 movement;                   // The vector to store the direction of the player's movement.
        Animator anim;                      // Reference to the animator component.
        Rigidbody playerRigidbody;          // Reference to the player's rigidbody.

        float distToGround;
        bool isTalking;

        DialogueCam dialogueCam;
        TPS_Cam tps_Cam;
        IUse currentlyUsed;

        void Awake()
        {
            // Set up references.
            anim = GetComponent<Animator>();
            playerRigidbody = GetComponent<Rigidbody>();
            distToGround = GetComponent<Collider>().bounds.extents.y;
            tps_Cam = Camera.main.GetComponent<TPS_Cam>();
            dialogueCam = Camera.main.GetComponent<DialogueCam>();
        }

        void Update()
        {

            if (isTalking && (Input.GetButtonDown("Jump") || Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0))
            {
                ChangeCam(false);
                if (currentlyUsed != null)
                    currentlyUsed.FinishUse();
            }

            if (Input.GetButtonDown("Jump") && isGrounded())
            {
                Invoke("Jump", 0.3f);
                anim.SetTrigger("Jump");
            }
            if (Input.GetButtonUp("Use") && !isTalking)
            {
                Collider[] colls = Physics.OverlapSphere(transform.position, interactMaxDistance);
                for (int i = 0; i < colls.Length; i++)
                {
                    if (colls[i].GetComponent<IUse>() != null)
                    {
                        if (colls[i].GetComponent<NPC>() != null)
                        {
                            anim.SetTrigger("Talk");
                            ChangeCam(true, colls[i].GetComponent<NPC>().cameraPositions);
                        }
                        else
                        {
                            anim.SetTrigger("Use");
                            isTalking = true;
                        }
                        currentlyUsed = colls[i].GetComponent<IUse>();

                        currentlyUsed.Use();
                        break;
                    }
                }
            }
        }
        void Jump()
        {
            playerRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        void ChangeCam(bool dialogue, List<GameObject> cameraPositions = null)
        {
            tps_Cam.enabled = !dialogue;
            dialogueCam.enabled = dialogue;
            isTalking = dialogue;
            if (dialogue)
            {
                dialogueCam.cameraPositions = cameraPositions;
                dialogueCam.ChangeCameraAngle();
            }
            else
            {
                GetComponent<ConversationManager>().EndDialogue();
            }

        }

        void FixedUpdate()
        {
            // Store the input axes.
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");

            Vector3 direction = new Vector3(h, 0, v);

            movement = Camera.main.transform.TransformDirection(direction);
            movement.y = 0;
            Vector3.Normalize(movement);
            // Move the player around the scene.
            Move();

            // Turn the player to face direction.
            Turning();

            if (playerRigidbody.velocity.y < 0)    //Fall faster when coming downwards
            {
                playerRigidbody.AddForce(Physics.gravity * 2f, ForceMode.Acceleration);
            }

            // Animate the player.
            Animating(h, v);
        }

        bool isGrounded()
        {
            return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
        }

        void Move()
        {
            // Normalise the movement vector and make it proportional to the speed per second.
            Vector3 move = movement * speed * Time.deltaTime;

            // Move the player to it's current position plus the movement.
            playerRigidbody.MovePosition(transform.position + move);
        }

        void Turning()
        {
            if (movement != Vector3.zero)
            {
                Quaternion lookRot = Quaternion.LookRotation(movement);
                playerRigidbody.rotation = Quaternion.Lerp(playerRigidbody.rotation, lookRot, 0.2f);
            }
        }

        void Animating(float h, float v)
        {
            // Create a boolean that is true if either of the input axes is non-zero.
            bool isMoving = h != 0f || v != 0f;

            // Tell the animator whether or not the player is walking.
            anim.SetBool("isMoving", isMoving);

        }
    }

}